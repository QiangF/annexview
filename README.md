Annexview is an Emacs Dired interface for modifying [git-annex
metadata][0] and for creating [metadata-driven views][1].  See the
[package commentary][2] for more details.

[0]: https://git-annex.branchable.com/metadata/
[1]: https://git-annex.branchable.com/tips/metadata_driven_views/
[2]: https://gitlab.com/kyleam/annexview/blob/master/annexview.el#L24
