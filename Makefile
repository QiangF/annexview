
-include config.mk

DASH_DIR ?= /dev/null
GHUB_DIR ?= /dev/null
WITH_EDITOR_DIR ?= /dev/null
MAGIT_POPUP_DIR ?= /dev/null
MAGIT_DIR ?= /dev/null

EMACSBIN ?= emacs

LOAD_PATH = -L $(DASH_DIR) -L $(WITH_EDITOR_DIR) -L $(GHUB_DIR) \
	    -L $(MAGIT_POPUP_DIR) -L $(MAGIT_DIR)
BATCH = $(EMACSBIN) -Q --batch $(LOAD_PATH)

all: annexview.elc annexview-autoloads.el

.PHONY: test
test: annexview.elc
	@$(BATCH) -L . -l annexview-tests.el \
	--eval "(ert-run-tests-batch-and-exit '(not (tag interactive)))"

.PHONY: clean
clean:
	$(RM) *.elc annexview-autoloads.el

%.elc: %.el
	@$(BATCH) -f batch-byte-compile $<

%-autoloads.el: %.el
	@$(BATCH) --eval \
	"(let ((make-backup-files nil)) \
	  (update-file-autoloads \"$(CURDIR)/$<\" t \"$(CURDIR)/$@\"))"
